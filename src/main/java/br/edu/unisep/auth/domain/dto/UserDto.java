package br.edu.unisep.auth.domain.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class UserDto {

    private final Integer id;

    private final String login;

    private final String name;

    private final List<String> roles;

}
